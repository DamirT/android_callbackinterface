package com.truedmp.applicationcallbackfunction;

public class Fiat implements CarInterface{

    private int horsePower;
    private String color;
    private String type;

    public Fiat(int horsePower, String color, String type) {
        this.horsePower = horsePower;
        this.color = color;
        this.type = type;
    }

    @Override
    public int getHorsePower() {
        return horsePower;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public String getType() {
        return type;
    }
}
