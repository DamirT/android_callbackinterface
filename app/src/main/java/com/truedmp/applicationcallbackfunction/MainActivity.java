package com.truedmp.applicationcallbackfunction;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.zip.Inflater;

public class MainActivity extends AppCompatActivity {

    TextView carHs;
    TextView carColor;
    TextView carType;

    Button citroenButton;
    Button fiatButton;
    Button opelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        carHs = findViewById(R.id.car_hs);
        carColor = findViewById(R.id.car_color);
        carType = findViewById(R.id.car_type);

        citroenButton = findViewById(R.id.citroen_button);
        fiatButton = findViewById(R.id.fiat_button);
        opelButton = findViewById(R.id.opel_button);

        citroenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Citroen citroen = new Citroen(100, "Blue", "Limusine");
                init(citroen);
            }
        });
        fiatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fiat fiat = new Fiat(90, "Black", "Hatchback");
                init(fiat);
            }
        });
        opelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Opel opel = new Opel(80, "White", "Hatchback");
                init(opel);
            }
        });
    }

    public void init(CarInterface carInterface) {
        if(carInterface instanceof Citroen) {
            carHs.setText("" + carInterface.getHorsePower());
            carColor.setText(carInterface.getColor());
            carType.setText(carInterface.getType());
            return;
        }
        if(carInterface instanceof Fiat) {
            carHs.setText("" + carInterface.getHorsePower());
            carColor.setText(carInterface.getColor());
            carType.setText(carInterface.getType());
            return;
        }
        if(carInterface instanceof Opel) {
            carHs.setText( "" + carInterface.getHorsePower());
            carColor.setText(carInterface.getColor());
            carType.setText(carInterface.getType());
            return;
        }
    }

}