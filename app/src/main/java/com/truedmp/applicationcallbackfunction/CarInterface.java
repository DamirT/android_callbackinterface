package com.truedmp.applicationcallbackfunction;

public interface CarInterface {

    int getHorsePower();

    String getColor();

    String getType();

}
